#include "program_3_shoes.h"

/*
  Girijesh Thodupunuri
  CS162
  Program 3

*/
// This file contains all the function definitions for program 3 regarding shoes

/*
  Function: menu
  Parameters: int & menu_response
  Purpose: Displays the menu options for the shoe collection program
           and prompts the user to select an option. The user's choice
           is stored in the menu_response variable.
*/
void menu(int & menu_response) {
  cout  << "1. Enter a new shoe \n"
        << "2. Display a match of particular shoe, by name \n"
        << "3. Display all shoes \n"
        << "4. Load from an external file \n"
        << "5. Save all items to an external data file \n"
        << "6. Quit Program \n"
        << "Please select one of the following options (1-5): ";
  cin >> menu_response;
  cin.ignore(100, '\n');
}

/*
  Function: display_shoes
  Parameters: shoe collection [], int num_of_shoes
  Purpose: Displays details of all shoes in the collection array.
*/
void display_shoes (shoe collection [], int num_of_shoes) {
  for(int i = 0; i < num_of_shoes; i++) {
    display_a_shoe(collection[i]);
  }
}

/*
  Function: add_a_shoe
  Parameters: shoe & a_shoe
  Purpose: Prompts the user to enter details of a new shoe,
           and stores the details in the provided shoe object.
*/
void add_a_shoe(shoe & a_shoe) {
  cout << "Please enter shoe name: ";
  cin.get(a_shoe.name, NAME, '\n');
  cout << "Name: " << a_shoe.name << endl;
  cin.ignore(100, '\n');

  cout << "Please enter description about shoe " << a_shoe.name << " : ";
  cin.get(a_shoe.description, DESCRIPTION, '\n');
  cin.ignore(100, '\n');

  cout << "Please enter the brand for " << a_shoe.name << " : ";
  cin.get(a_shoe.brand, BRAND, '\n');
  cin.ignore(100, '\n');

  cout << "Please enter the price " << a_shoe.name << "was purchased at: $";
  cin >> a_shoe.purchase_price;
  cin.ignore(100, '\n');

  cout << "Please enter the current market value of " << a_shoe.name << " : $";
  cin >> a_shoe.market_price;
  cin.ignore(100, '\n');
}

/*
  Function: display_a_shoe
  Parameters: shoe & a_shoe
  Purpose: Displays details of a single shoe.
*/
void display_a_shoe( shoe & a_shoe) {
  cout << "Name: " << a_shoe.name << endl;
  cout << "Description: " << a_shoe.description << endl;
  cout << "Brand: " << a_shoe.brand << endl;
  cout << "Purchase Price: $" <<  a_shoe.purchase_price << endl;
  cout << "Market price: $" << a_shoe.market_price << endl;
  cout << "_ _ _ _ _ _ _ _ _ _ _ _ _ \n";
}

/*
  Function: to_lower
  Parameters: char name[]
  Purpose: Converts all characters in the provided string to lowercase.
*/
void to_lower(char name[]) {
  for(int i = 0; i < strlen(name); i++) {
    name[i] = tolower(name[i]);
  }
}

/*
  Function: find_shoe
  Parameters: char name[], shoe collection[], int num_of_shoes
  Purpose: Searches for a shoe with the specified name in the collection array.
           If a match is found, the details of the matching shoe are displayed.
*/
void find_shoe (char name[], shoe collection[], int num_of_shoes) {
  to_lower(name);
  for(int i = 0; i < num_of_shoes; i++) {
    char copy_name[NAME]; // Variable to store a copy of the shoe name for case-insensitive comparison
    strcpy(copy_name, collection[i].name);
    to_lower(copy_name);
    if(strcmp(name, copy_name) == 0) {
      cout << "Match found: \n";
      display_a_shoe(collection[i]);
    }
  }
}
