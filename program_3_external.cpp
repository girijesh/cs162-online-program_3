#include "program_3_shoes.h"

/*
  Girijesh Thodupunuri
  CS162
  Program 3

*/
// This file contains all the function definitions for program 3 regarding saving and loading from external files

/*
  Function: save
  Parameters: shoe collection [], int num_of_shoes
  Purpose: Saves the shoe collection to an external file named "shoes.txt".
           Each shoe's details are written to the file in a specific format.
*/
void save(shoe collection [], int num_of_shoes) {
  ofstream file_out; // Output file stream variable used for writing data to a file
  file_out.open("shoes.txt");
  if(file_out) {
    for(int i = 0; i < num_of_shoes; i++) {
      file_out << collection[i].name << '|' << collection[i].brand << '|' << collection[i].description << '|' << collection[i].purchase_price << '|' << collection[i].market_price << endl;
    }
  }
  file_out.close();
}

/*
  Function: load
  Parameters: shoe collection [], int & num_of_shoes
  Purpose: Loads the shoe collection from an external file named "shoes.txt".
           Each line in the file represents a shoe's details, which are read
           and stored in the collection array.
*/
void load(shoe collection [], int & num_of_shoes){
  ifstream file_in; // Input file stream variable used for reading data from a file 

  file_in.open("shoes.txt");
  if(file_in) {
    file_in.get(collection[num_of_shoes].name, NAME, '|');
    file_in.ignore(100, '|');
    while(!file_in.eof() && num_of_shoes < SHOES) {
      file_in.get(collection[num_of_shoes].brand, BRAND, '|');
      file_in.ignore(100, '|');

      file_in.get(collection[num_of_shoes].description, DESCRIPTION, '|');
      file_in.ignore(100, '|');

      file_in >> collection[num_of_shoes].purchase_price;
      file_in.ignore(100, '|');

      file_in >> collection[num_of_shoes].market_price;
      file_in.ignore(100, '\n');
      num_of_shoes++;
      file_in.get(collection[num_of_shoes].name, NAME, '|');
      file_in.ignore(100, '|');

    }
    file_in.close();
  } else {
    cout << "Error opening file or file doesn't exist";
  }
}
