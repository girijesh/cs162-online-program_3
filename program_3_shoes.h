#include <iostream>
#include <cctype>
#include <cstring>
#include <fstream> 
using namespace std;

/*
  Girjesh Thodupunuri
  CS162
  Program 3
*/
// This file contains the constant variables, struct and prototypes for the program 3

// constants

const int NAME = 31;         // Maximum length of a shoe name
const int BRAND = 31;        // Maximum length of a shoe brand
const int DESCRIPTION = 131; // Maximum length of a shoe description
const int SHOES = 100;       // Maximum number of shoes in the collection

struct shoe
{
  /* data */
  char name[NAME];            // Array to store the name of the shoe
  char brand[BRAND];          // Array to store the brand of the shoe
  char description[DESCRIPTION]; // Array to store the description of the shoe
  float purchase_price;       // Price at which the shoe was purchased
  float market_price;         // Current market price of the shoe
};

// Prototypes
void menu(int & menu_response);
void add_a_shoe(shoe & a_shoe);
void display_shoes (shoe collection [], int num_of_shoes);
void display_a_shoe( shoe & a_shoe);
void load(shoe collection [], int & num_of_shoes);
void save(shoe collection [], int num_of_shoes);
void to_lower(char name[]);
void find_shoe (char name[], shoe collection[], int num_of_shoes);
