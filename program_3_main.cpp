#include "program_3_shoes.h"

/*
  Girijesh Thodupunuri
  CS162
  Program 3

*/
// This file contains all the main operations for the program 3 regarding shoes

int main()
{
  shoe collection [SHOES]; // Array to store the collection of shoes
  int num_of_shoes = 0;    // Number of shoes currently in the collection
  int menu_response;       // User's menu choice
  
  while (menu_response != 6) {
    menu(menu_response); // Display the menu and get user's choice
    if(menu_response < 1 || menu_response > 6) {
      cout<< "Please Enter a valid choice";
      menu(menu_response);
    }

    if(menu_response == 1 && num_of_shoes < SHOES) {
      // Add a new shoe to the collection
      add_a_shoe(collection[num_of_shoes]);
      cout << collection[num_of_shoes].name << " has been added to the collection \n";
      num_of_shoes++;
    }
    if( menu_response == 2) {
      // Find a match for a specific shoe
      char name[NAME]; // Variable to store the name of the shoe to be found
      cout<< "Enter the name of the shoe you are looking for: ";
      cin.get(name, NAME, '\n');
      cin.ignore(100, '\n');
      find_shoe(name, collection, num_of_shoes);
    }

    if(menu_response == 3 ) {
      // Display all shoes in the collection
      display_shoes(collection, num_of_shoes);
    }

    if(menu_response == 4) {
      // Load data from an external file
      load(collection, num_of_shoes);
    }

    if(menu_response == 5) {
      // Save data to an external file
      save(collection, num_of_shoes);
    }
  }
  return 0;
}
